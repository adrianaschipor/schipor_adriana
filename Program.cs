﻿using System;
using System.Drawing;
using OpenTK;
using OpenTK.Graphics.OpenGL;
using OpenTK.Input;

namespace laborator_egc
{
	class SimpleWindow : GameWindow
	{

		float xCube = 0;
		float yCube = 0;
		int xPerspective = 0;
		int yPerspective = 0;
		int zPerspective = 0;
		int xAngle = 0;
		int yAngle = 0;
		int zAngle = 0;
		bool displayCube = true;
		KeyboardState lastKeyPressed;

		// Constructor.
		public SimpleWindow() : base(800, 600)
		{
			//KeyDown += Keyboard_KeyDown;
			VSync = VSyncMode.On;
		}

		// Tratează evenimentul generat de apăsarea unei taste. Mecanismul standard oferit de .NET
		// este cel utilizat.
		//       void Keyboard_KeyDown(object sender, KeyboardKeyEventArgs e)
		//       {
		//           if (e.Key == Key.Escape)
		//               this.Exit();

		//            if (e.Key == Key.F11)
		//               if (this.WindowState == WindowState.Fullscreen)
		//                  this.WindowState = WindowState.Normal;
		//             else
		//                 this.WindowState = WindowState.Fullscreen;
		//      }

		// Setare mediu OpenGL și încarcarea resurselor (dacă e necesar)
		protected override void OnLoad(EventArgs e)
		{
			base.OnLoad(e);

			GL.ClearColor(Color.Gray);
			GL.Enable(EnableCap.DepthTest);
			this.CursorGrabbed = true;
			this.CursorVisible = false;
		}

		// Inițierea afișării și setarea viewport-ului grafic.
		protected override void OnResize(EventArgs e)
		{
			base.OnResize(e);
			GL.Viewport(0, 0, Width, Height);
			double aspect_ratio = Width / (double)Height;
			Matrix4 perspective = Matrix4.CreatePerspectiveFieldOfView(MathHelper.PiOver3, (float)aspect_ratio, 1, 64);
			GL.MatrixMode(MatrixMode.Projection);
			GL.LoadMatrix(ref perspective);
		}

		// Secțiunea pentru "game logic"/"business logic".
		protected override void OnUpdateFrame(FrameEventArgs e)
		{
			base.OnUpdateFrame(e);

			KeyboardState keyboard = Keyboard.GetState();
			if (keyboard[Key.Escape])
			{
				Exit();
				return;
			}
			else if (keyboard[Key.ControlLeft] && keyboard[Key.P] && !keyboard.Equals(lastKeyPressed))
			{
				if (displayCube == true)
				{
					displayCube = false;
				}
				else
				{
					displayCube = true;
				}
			}
			else if (keyboard[Key.F11])
			{
				if (WindowState == WindowState.Fullscreen)
					WindowState = WindowState.Normal;
				else
					WindowState = WindowState.Fullscreen;
			}
			else if (keyboard[Key.R])
			{
				// Reset button
				xPerspective = yPerspective = 0;
			}

			if (keyboard[Key.Up])
			{
				yPerspective += 1;
			}
			else if (keyboard[Key.Down])
			{
				yPerspective -= 1;
			}

			if (keyboard[Key.Left])
			{
				xPerspective += 1;
			}
			else if (keyboard[Key.Right])
			{
				xPerspective -= 1;
			}

			lastKeyPressed = keyboard;
		}
		protected override void OnMouseMove(MouseMoveEventArgs e)
		{
			base.OnMouseMove(e);
			xCube = (e.X - Width / 2f) / (Width / 2f);
			yCube = -(e.Y - Height / 2f) / (Height / 2f);
		}

		// Secțiunea pentru randarea scenei 3D. Controlată de modulul logic din metoda ONUPDATEFRAME.
		// Parametrul de intrare "e" conține informatii de timing pentru randare.
		protected override void OnRenderFrame(FrameEventArgs e)
		{
			// GL.Clear(ClearBufferMask.ColorBufferBit);

			base.OnRenderFrame(e);

			GL.Clear(ClearBufferMask.ColorBufferBit | ClearBufferMask.DepthBufferBit);

			Matrix4 lookat = Matrix4.LookAt(xPerspective, yPerspective, zPerspective, 0, 0, 0, 0, 1, 0);
			GL.MatrixMode(MatrixMode.Modelview);
			GL.LoadMatrix(ref lookat);

			//draw axex
			GL.Begin(PrimitiveType.Lines);

			// axa X
			GL.Color3(Color.Red);
			GL.Vertex3(0, 0, 0);
			GL.Vertex3(30, 0, 0);

			// axa Y
			GL.Color3(Color.Blue);
			GL.Vertex3(0, 0, 0);
			GL.Vertex3(0, 30, 0);

			// axa Z
			GL.Color3(Color.Yellow);
			GL.Vertex3(0, 0, 0);
			GL.Vertex3(0, 0, 30);


			GL.End();

			if (displayCube == true)
			{
				DrawCube();
			}

			SwapBuffers();

			this.SwapBuffers();
		}

		private void DrawCube()
		{
			GL.Begin(PrimitiveType.Quads);

			GL.Color3(Color.Silver);
			GL.Vertex3(xCube - 1.0f, yCube - 1.0f, -1.0f);
			GL.Vertex3(xCube - 1.0f, yCube + 1.0f, -1.0f);
			GL.Vertex3(xCube + 1.0f, yCube + 1.0f, -1.0f);
			GL.Vertex3(xCube + 1.0f, yCube - 1.0f, -1.0f);

			GL.Color3(Color.Honeydew);
			GL.Vertex3(xCube - 1.0f, yCube - 1.0f, -1.0f);
			GL.Vertex3(xCube + 1.0f, yCube - 1.0f, -1.0f);
			GL.Vertex3(xCube + 1.0f, yCube - 1.0f, 1.0f);
			GL.Vertex3(xCube - 1.0f, yCube - 1.0f, 1.0f);

			GL.Color3(Color.Moccasin);

			GL.Vertex3(xCube - 1.0f, yCube - 1.0f, -1.0f);
			GL.Vertex3(xCube - 1.0f, yCube - 1.0f, 1.0f);
			GL.Vertex3(xCube - 1.0f, yCube + 1.0f, 1.0f);
			GL.Vertex3(xCube - 1.0f, yCube + 1.0f, -1.0f);

			GL.Color3(Color.IndianRed);
			GL.Vertex3(xCube - 1.0f, yCube - 1.0f, 1.0f);
			GL.Vertex3(xCube + 1.0f, yCube - 1.0f, 1.0f);
			GL.Vertex3(xCube + 1.0f, yCube + 1.0f, 1.0f);
			GL.Vertex3(xCube - 1.0f, yCube + 1.0f, 1.0f);

			GL.Color3(Color.PaleVioletRed);
			GL.Vertex3(xCube - 1.0f, yCube + 1.0f, -1.0f);
			GL.Vertex3(xCube - 1.0f, yCube + 1.0f, 1.0f);
			GL.Vertex3(xCube + 1.0f, yCube + 1.0f, 1.0f);
			GL.Vertex3(xCube + 1.0f, yCube + 1.0f, -1.0f);

			GL.Color3(Color.ForestGreen);
			GL.Vertex3(xCube + 1.0f, yCube - 1.0f, -1.0f);
			GL.Vertex3(xCube + 1.0f, yCube + 1.0f, -1.0f);
			GL.Vertex3(xCube + 1.0f, yCube + 1.0f, 1.0f);
			GL.Vertex3(xCube + 1.0f, yCube - 1.0f, 1.0f);

			GL.End();
		}

		[STAThread]
		static void Main(string[] args)
		{
			using (SimpleWindow example = new SimpleWindow())
			{
				example.Run(60.0, 0.0);
			}
		}
	}
}
